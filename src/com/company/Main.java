package com.company;

public class Main {

    public static void main(String[] args) {
	    King king = new King();
	    Queen queen = new Queen();
	    Troll troll = new Troll();
	    Knight knight = new Knight();

	    king.fight();
	    queen.fight();
	    knight.fight();
	    troll.fight();

	    troll.setWeapon(new SwordBehavior());
	    troll.fight();
    }
}
